package ru.ovrays.commandsapi.managers

import org.bukkit.command.CommandSender
import ru.ovrays.commandsapi.Command
import java.util.*

class CommandManager {
    private val commands: MutableList<Command>

    fun add(command: Command) {
        commands.add(command)
    }

    fun remove(command: Command) {
        commands.removeIf { it == command }
    }

    fun runCommand(name: String, sender: CommandSender, args: Array<String>): Boolean {
        val commandName = name.toLowerCase()

        commands.firstOrNull { it.name == commandName || commandName in it.aliases }
            ?.execute(sender, args) ?: return false
            return true
    }

    init {
        commands = ArrayList<Command>()
    }
}

