package ru.ovrays.commandsapi.example

import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import ru.ovrays.commandsapi.Command
import ru.ovrays.commandsapi.parameters.Parameter
import ru.ovrays.commandsapi.parameters.ParameterPool
import ru.ovrays.commandsapi.parameters.types.TypeDouble
import ru.ovrays.commandsapi.parameters.types.TypePlayer
import ru.ovrays.commandsapi.utils.getPlayer
import ru.ovrays.commandsapi.utils.sendText

class HealthCommand: Command(
    "health",
    listOf("hp"),
    optionalParameters = listOf(
        Parameter<Player>("player", "Target name", TypePlayer),
        Parameter<Double>("value", "Hp value", TypeDouble)
    )
) {
    override fun run(sender: CommandSender, args: Array<String>, parameters: ParameterPool) {
        val player = parameters.getOptionalParameter<Player>("player") { sender.getPlayer() }
        val hp = parameters.getOptionalParameter<Double>("value", 20.0)

        player.health = hp

        sender.sendText("&b${player.name} health setuped to &c${hp}.")
    }
}