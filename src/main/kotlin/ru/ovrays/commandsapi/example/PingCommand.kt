package ru.ovrays.commandsapi.example

import org.bukkit.command.CommandSender
import ru.ovrays.commandsapi.Command
import ru.ovrays.commandsapi.parameters.Parameter
import ru.ovrays.commandsapi.parameters.ParameterPool
import ru.ovrays.commandsapi.parameters.types.TypeString
import ru.ovrays.commandsapi.requirements.PermissionRequirement

class PingCommand: Command(
    "ping"
) {
    override fun run(sender: CommandSender, args: Array<String>, parameters: ParameterPool) {
        sender.sendMessage("Pong!")
    }
}
