package ru.ovrays.commandsapi.parameters.types

import ru.ovrays.commandsapi.exceptions.CommandException
import ru.ovrays.commandsapi.utils.catch

object TypeDouble : IType<Double> {
    @Throws(CommandException::class)
    override fun readOrNull(string: String): Double {
        return catch(errorMessage) {
            string.toInt().toDouble()
        }
    }

    override var errorMessage: String = "Недопустимое числовое значение."
}

