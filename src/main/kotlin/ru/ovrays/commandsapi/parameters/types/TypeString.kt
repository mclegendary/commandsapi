package ru.ovrays.commandsapi.parameters.types

import ru.ovrays.commandsapi.exceptions.CommandException

object TypeString : IType<String> {
    @Throws(CommandException::class)
    override fun readOrNull(string: String): String {
        return string
    }

    override var errorMessage: String = "Недопустимое строчное значение."
}
