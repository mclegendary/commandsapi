package ru.ovrays.commandsapi.parameters.types

import org.apache.commons.lang.math.DoubleRange
import org.apache.commons.lang.math.Range
import ru.ovrays.commandsapi.exceptions.CommandException

interface IType <T> {
    var errorMessage: String

    @Throws(CommandException::class)
    fun readOrNull(string: String): T?
}