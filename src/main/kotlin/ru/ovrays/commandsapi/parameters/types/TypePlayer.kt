package ru.ovrays.commandsapi.parameters.types

import org.bukkit.entity.Player
import ru.ovrays.commandsapi.utils.getPlayer

object TypePlayer : IType<Player> {
    override fun readOrNull(string: String): Player {
        return getPlayer(string)
    }

    override var errorMessage: String = "Игрок не найден."
}