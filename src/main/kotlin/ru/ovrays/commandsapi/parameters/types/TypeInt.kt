package ru.ovrays.commandsapi.parameters.types

import ru.ovrays.commandsapi.exceptions.CommandException
import ru.ovrays.commandsapi.utils.catch

object TypeInt : IType<Int> {
    @Throws(CommandException::class)
    override fun readOrNull(string: String): Int {
        return catch(errorMessage) {
            string.toInt()
        }
    }

    override var errorMessage: String = "Недопустимое числовое значение."
}