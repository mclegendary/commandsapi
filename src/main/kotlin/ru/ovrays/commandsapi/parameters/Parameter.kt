package ru.ovrays.commandsapi.parameters

import ru.ovrays.commandsapi.parameters.types.IType

class Parameter <T>(
    var name: String,
    var description: String = "",
    var type: IType<T>,
    var allowTailArguments: Boolean = false
)