package ru.ovrays.commandsapi.parameters

import ru.ovrays.commandsapi.exceptions.CommandException
import java.lang.Integer.min

class ParameterPool {

    var requiredParameters: MutableMap<String, String> = mutableMapOf()
    var optionalParameters: MutableMap<String, String> = mutableMapOf()

    private var parametersList: List<Parameter<*>> = listOf()
    private var hasTailParameters: Boolean = false

    fun resolve(args: List<String>, params: List<Parameter<*>>, required: Boolean): Map<String, String> {

        if(required) {
            if(args.size < params.size) throw CommandException("Недостаточно аргуменов.")
        }

        val len = min(args.size, params.size)

        val ps = params.take(len).map { it.name }
        val rs = args.take(len)
        val es = args.drop(len)

        val zs = ps.zip(rs)

        val parameters: MutableMap<String, String> = mutableMapOf()

        parameters.putAll(zs)
        parametersList = parametersList + params

        if (args.size > params.size && params.any { it.allowTailArguments }) {
            val target = params.last { it.allowTailArguments }.name

            parameters[target] = parameters[target] + " " + es.joinToString(" ")
            hasTailParameters = true
        }

        if (required)
            requiredParameters = parameters
        else
            optionalParameters = parameters

        return if (required) requiredParameters else optionalParameters
    }

    fun hasOptionalParameter(name: String): Boolean = optionalParameters.any { it.key == name && it.value.isNotBlank() }

    fun hasOptionalParameters(): Boolean = optionalParameters.isNotEmpty()

    fun hasTailParameters(): Boolean = hasTailParameters

    fun <T> getOptionalParameter(name: String, defaultValue: T): T = getOptionalParameter(name) { defaultValue }

    fun <T> getOptionalParameter(name: String, defaultValue: () -> T): T {
        val value = optionalParameters[name] ?: return defaultValue()

        val target = parametersList.firstOrNull { it.name == name } ?: return defaultValue()

        return target.type.readOrNull(value) as? T ?: return defaultValue()
    }

    @Throws(CommandException::class)
    fun <T> getRequiredParameter(name: String): T {
        val value = requiredParameters[name] ?: throw CommandException("Неизвестный аргумент: $name")

        val target = parametersList.firstOrNull { it.name == name } ?: throw CommandException("Неизвестный аргумент: $name")

        return target.type.readOrNull(value) as? T ?: throw CommandException(target.type.errorMessage)
    }
}
