package ru.ovrays.commandsapi

import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.plugin.java.JavaPlugin
import ru.ovrays.commandsapi.example.HealthCommand
import ru.ovrays.commandsapi.example.PingCommand
import ru.ovrays.commandsapi.managers.CommandManager
import ru.ovrays.commandsapi.utils.sendError
import java.util.logging.Logger

class CommandsAPI: JavaPlugin() {
    init {
        instance = this
        log = logger
    }

    override fun onEnable() {
        commandManager = CommandManager()
        commandManager.add(PingCommand())
        commandManager.add(HealthCommand())
    }

    override fun onCommand(
        sender: CommandSender,
        command: Command,
        label: String,
        args: Array<String>
    ): Boolean {
        if(commandManager.runCommand(label, sender, args))
            return true
        else
            sender.sendError("Неизвестная команда.")
            return false
    }

    companion object {
        lateinit var instance: CommandsAPI
        lateinit var commandManager: CommandManager
        lateinit var log: Logger
    }
}