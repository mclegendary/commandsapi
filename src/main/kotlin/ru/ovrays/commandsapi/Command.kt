package ru.ovrays.commandsapi

import org.bukkit.command.CommandSender
import ru.ovrays.commandsapi.exceptions.CommandException
import ru.ovrays.commandsapi.parameters.Parameter
import ru.ovrays.commandsapi.parameters.ParameterPool
import ru.ovrays.commandsapi.requirements.IRequirement
import ru.ovrays.commandsapi.utils.sendError
import ru.ovrays.commandsapi.utils.sendText

abstract class Command(
    val name: String,
    val aliases: List<String> = listOf(),

    private val requirements: List<IRequirement> = listOf(),

    private val requiredParameters: List<Parameter<*>> = listOf(),
    private val optionalParameters: List<Parameter<*>> = listOf()
    ) {
    fun execute(sender: CommandSender, args: Array<String>) {
        for(requirement in requirements.filterNot { it.check(sender) }) {
            sender.sendError(requirement.errorMessage)
            return
        }

        if (args.isEmpty() && requiredParameters.isNotEmpty()) {
            this.sendHelp(sender)
        } else
            try {
                val parameterPool = ParameterPool()

                val required = parameterPool.resolve(args.toList(), requiredParameters, true)
                parameterPool.resolve(args.drop(required.size), optionalParameters, false)

                this.run(sender, args, parameterPool)
            } catch (ex: Exception) {
                sender.sendError(ex.localizedMessage)
            }
    }

    @Throws(CommandException::class)
    abstract fun run(sender: CommandSender, args: Array<String>, parameters: ParameterPool)

    fun sendHelp(sender: CommandSender) {
        var message = "&cUsage: /${this.name}"

        this.requiredParameters.forEach {
            message += " <${it.name}>"
        }

        this.optionalParameters.forEach {
            message += " [${it.name}]"
        }

        sender.sendText(message)
    }
}