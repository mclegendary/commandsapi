package ru.ovrays.commandsapi.exceptions

class CommandException(override val message: String): Exception()