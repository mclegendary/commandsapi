package ru.ovrays.commandsapi.utils

import org.bukkit.ChatColor
import org.bukkit.command.CommandSender
import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player
import ru.ovrays.commandsapi.CommandsAPI
import ru.ovrays.commandsapi.exceptions.CommandException

fun CommandSender.isPlayer(): Boolean {
    return this is Player
}

fun CommandSender.isConsole(): Boolean {
    return this is ConsoleCommandSender
}

fun CommandSender.sendText(text: String) {
    this.sendMessage(ChatColor.translateAlternateColorCodes('&', text))
}

fun CommandSender.sendError(error: String) {
    this.sendText("&c$error")
}

fun CommandSender.getPlayer(): Player {
    return if (this.isPlayer()) this as Player else throw CommandException("Данную команду можно применить лишь на игрока.")
}

fun getPlayer(name: String): Player {
    return CommandsAPI.instance.server.getPlayer(name) ?: throw CommandException("Игрок не найден.")
}

fun <T> catch(errorMessage: String, block: () -> T): T {
    return try {
        block()
    } catch (e: Exception) {
        throw CommandException(errorMessage)
    }
}