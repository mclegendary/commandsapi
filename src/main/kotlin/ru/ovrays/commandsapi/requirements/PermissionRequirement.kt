package ru.ovrays.commandsapi.requirements

import org.bukkit.command.CommandSender

class PermissionRequirement(var permission: String, override var errorMessage: String = "Недостаточно прав.") :
    IRequirement {
    override fun check(sender: CommandSender): Boolean {
        return sender.hasPermission(permission)
    }
}