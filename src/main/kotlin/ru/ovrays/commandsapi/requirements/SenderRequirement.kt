package ru.ovrays.commandsapi.requirements

import org.bukkit.command.CommandSender
import ru.ovrays.commandsapi.utils.isConsole
import ru.ovrays.commandsapi.utils.isPlayer

enum class SenderRequirement: IRequirement {
    CONSOLE {
        override fun check(sender: CommandSender): Boolean {
            return sender.isConsole()
        }

        override var errorMessage: String = "Данная команда может быть исполнена лишь из консоли."
    },
    PLAYER {
        override fun check(sender: CommandSender): Boolean {
            return sender.isPlayer()
        }

        override var errorMessage: String = "Данная команда может быть исполнена лишь игроком."
    }
}