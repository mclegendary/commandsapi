package ru.ovrays.commandsapi.requirements

import org.bukkit.command.CommandSender

interface IRequirement {

    fun check(sender: CommandSender): Boolean

    var errorMessage: String

}